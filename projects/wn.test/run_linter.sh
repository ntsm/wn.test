#!/bin/bash
./vendor/bin/phpcs --standard=PSR12 --ignore=./tests,./config,./server.php,./bootstrap,./public,./vendor,./storage,./resources,./database,./_ide_helper_models.php,./_ide_helper.php -s --colors ./
