<?php

use App\Models\Book;
use Faker\Factory;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->faker = Factory::create();
    }

    /**
     * @test
     * @dataProvider getPathDataForPositiveTest
     */
    public function positive($uri, $method, $data)
    {
        if ($method === 'get') {
            $response = $this->get(
                $uri,
                [
                    'Authorization' => 'Bearer ' . env('JWT_SECRET')
                ]
            );
        } else if (in_array($method, ['post', 'patch'])) {
            $response = $this->$method(
                $uri,
                $data,
                [
                    'Authorization' => 'Bearer ' . env('JWT_SECRET')
                ]
            );
        }


        $response->assertStatus(200);
    }

    /**
     * @return \string[][]
     */
    public function getPathDataForPositiveTest()
    {
        return [
            [
                'uri' => '/api/v1/books',
                'method' => 'get',
                'data' => []
            ],
            [
                'uri' => '/api/v1/books',
                'method' => 'post',
                'data' => [
                    'title' => $this->faker->title
                ]
            ],
            [
                'uri' => '/api/v1/books/1',
                'method' => 'patch',
                'data' => [
                    'title' => $this->faker->title
                ]
            ],
            [
                'uri' => '/api/v1/books/1',
                'method' => 'get',
                'data' => []
            ],
            [
                'uri' => '/api/v1/authors/1/books',
                'method' => 'get',
                'data' => []
            ],
            [
                'uri' => '/api/v1/authors',
                'method' => 'post',
                'data' => [
                    'name' => $this->faker->name
                ]
            ],
        ];
    }
}
