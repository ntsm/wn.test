<?php

use App\Http\Requests\Api\V1\BookListRequest;
use Faker\Factory;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

class BookListRequestTest extends TestCase
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * AuthorCreateRequestTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    /**
     * @return mixed|string[]
     */
    public function getRules()
    {
        return (new BookListRequest())->rules();
    }

    /**
     * @test
     * @dataProvider getDataFieldValidation
     */
    public function fieldValidation($passed, $data)
    {
        $validator = Validator::make($data, $this->getRules());

        $this->assertEquals(
            $passed,
            !$validator->fails()
        );
    }

    /**
     * @return array[]
     */
    public function getDataFieldValidation()
    {
        return [
            'positive_test' => [
                'passed' => true,
                'data' => [
                    'offset' => $this->faker->randomDigit(),
                ],
            ],
            'negative_test' => [
                'passed' => false,
                'data' => [
                    'offset' => $this->faker->text(),
                ],
            ],
        ];
    }
}
