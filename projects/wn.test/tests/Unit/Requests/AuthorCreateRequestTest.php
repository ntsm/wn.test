<?php

use App\Http\Requests\Api\V1\AuthorCreateRequest;
use Faker\Factory;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

class AuthorCreateRequestTest extends TestCase
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * AuthorCreateRequestTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    /**
     * @return mixed|string[]
     */
    public function getRules()
    {
        return (new AuthorCreateRequest())->rules();
    }

    /**
     * @test
     * @dataProvider getDataFieldValidation
     */
    public function fieldValidation($passed, $data)
    {
        $validator = Validator::make($data, $this->getRules());

        $this->assertEquals(
            $passed,
            !$validator->fails()
        );
    }

    /**
     * @return array[]
     */
    public function getDataFieldValidation()
    {
        return [
            'positive_test' => [
                'passed' => true,
                'data' => [
                    'name' => $this->faker->realTextBetween(1,255),
                ],
            ],
            'positive_test2' => [
                'passed' => true,
                'data' => [
                    'name' => $this->faker->realTextBetween(1,255),
                    'books' => [
                        $this->faker->randomDigit(),
                        $this->faker->randomDigit(),
                    ],
                ],
            ],
            'books_id_is_string' => [
                'passed' => false,
                'data' => [
                    'name' => $this->faker->realTextBetween(1,255),
                    'books' => [
                        $this->faker->text(),
                    ],
                ],
            ],
            'wrong_min_lenght' => [
                'passed' => false,
                'data' => [
                    'name' => '1'
                ],
            ],
            'empty' => [
                'passed' => false,
                'data' => [
                ],
            ],
            'not_string' => [
                'passed' => false,
                'data' => [
                    'name' => $this->faker->randomNumber()
                ],
            ],
            'wrong_max_lenght' => [
                'passed' => false,
                'data' => [
                    'name' => $this->faker->realTextBetween(255,1000)
                ],
            ],
        ];
    }
}
