<?php

use App\DTO\BookUpsertDTO;
use App\Models\Book;
use App\Services\BookService;
use Faker\Factory;
use Tests\TestCase;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\Author;
use App\DTO\BookListDTO;

class BookServiceTest extends TestCase
{
    /**
     * @var \Faker\Generator
     */
    private $faker;
    private $service;
    private $setUpBooksModel;
    private $setUpAuthor1;

    /**
     * AuthorCreateRequestTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
        $this->service = new BookService();
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpBooksModel = Book::factory(10)->create();
        $this->setUpAuthor1 = Author::factory()->create();
    }

    /**
     * @test
     */
    public function createPositive()
    {
        $data = [
            'title' => $this->faker->text(255),
            'authors' => []
        ];
        $dto = new BookUpsertDTO($data);
        $model = $this->service->create($dto);
        $expected = Book::find($model->id);
        $this->assertEquals($expected->getAttributes(), $model->getAttributes());
    }

    /**
     * @test
     */
    public function updatePositive()
    {
        $data = [
            'title' => $this->faker->text(255),
            'authors' => [$this->setUpAuthor1->id]
        ];
        $dto = new BookUpsertDTO($data);
        $model = $this->service->update($this->setUpBooksModel[0], $dto);
        $expected = Book::find($model->id);

        $this->assertEquals($expected->getAttributes(), $model->getAttributes());
        $this->assertEquals($expected->authors->toArray(), [$this->setUpAuthor1->toArray()]);
    }

    /**
     * @test
     */
    public function showMethodPositive()
    {
        $model = $this->service->get($this->setUpBooksModel[0]->id);
        $this->assertEquals($this->setUpBooksModel[0]->id, $model->id);
    }

    /**
     * @test
     */
    public function showMethodNegative()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->service->get(999999999999);
    }

    /**
     * @test
     */
    public function listMethodPositive()
    {
        $dto = new BookListDTO([
            'offset' => 10,
            'limit' => 10
        ]);

        $this->assertEquals(
            $this->service->list($dto)->toArray(),
            $this->setUpBooksModel->toArray()
        );
    }

    /**
     * @test
     */
    public function listMethodPositiveLimit()
    {
        $dto = new BookListDTO([
            'offset' => 10,
            'limit' => 1
        ]);

        $this->assertEquals($this->service->list($dto)->toArray(), [$this->setUpBooksModel[0]->toArray()]);
    }

    /**
     * @test
     */
    public function listMethodPositiveOffset()
    {
        $dto = new BookListDTO([
            'offset' => 11,
            'limit' => 1
        ]);

        $this->assertEquals($this->service->list($dto)->toArray(), [$this->setUpBooksModel[1]->toArray()]);
    }
}
