<?php

use App\DTO\AuthorCreateDTO;
use App\Models\Author;
use App\Services\AuthorService;
use Faker\Factory;
use Tests\TestCase;
use App\Models\Book;

class AuthorServiceTest extends TestCase
{
    /**
     * @var \Faker\Generator
     */
    private $faker;
    private $service;
    private $setUpBookModel;

    /**
     * AuthorCreateRequestTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
        $this->service = new AuthorService();
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpBookModel = Book::factory()->create();
    }

    /**
     * @test
     */
    public function createPositiveTest()
    {
        $data = [
            'name' => $this->faker->text(255),
            'books' => [$this->setUpBookModel->id]
        ];
        $dto = new AuthorCreateDTO($data);
        $model = $this->service->create($dto);
        $expected = Author::find($model->id);
        $this->assertEquals($expected->getAttributes(), $model->getAttributes());
        $this->assertEquals($expected->books->toArray(), [$this->setUpBookModel->toArray()]);
    }

    /**
     * @test
     */
    public function createPositiveTestEmptyBooks()
    {
        $data = [
            'name' => $this->faker->text(255),
            'books' => []
        ];
        $dto = new AuthorCreateDTO($data);
        $model = $this->service->create($dto);
        $expected = Author::find($model->id);
        $this->assertEquals($expected->getAttributes(), $model->getAttributes());
    }

    /**
     * @test
     */
    public function createNegativeTest()
    {
        $this->expectException(Exception::class);
        $data = [
            'name' => $this->faker->text(255),
            'books' => [
                99999999999999999999999
            ]
        ];
        $dto = new AuthorCreateDTO($data);
        $this->service->create($dto);
    }

    /**
     * @test
     */
    public function getAuthorBooksPositive()
    {
        $books = Book::factory(10)->create();
        $data = [
            'name' => $this->faker->text(255),
            'books' => $books->pluck('id')->toArray()
        ];
        $dto = new AuthorCreateDTO($data);
        $model = $this->service->create($dto);

        $expected = $this->service->getAuthorBooks($model->id);

        $this->assertEquals($expected->toArray(), $model->books->toArray());
    }
}
