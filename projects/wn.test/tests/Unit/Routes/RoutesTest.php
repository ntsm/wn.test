<?php

use App\Http\Controllers\Api\V1\AuthorController;
use App\Http\Controllers\Api\V1\BookController;
use Tests\TestCase;

class RoutesTest extends TestCase
{
    /**
     * @return array
     */
    public function getActions(): array
    {
        return [
            [
                'pattern' => 'api/v1/books',
                'action' => BookController::class . '@create',
                'method' => 'POST'
            ],
            [
                'pattern' => 'api/v1/books',
                'action' => BookController::class . '@list',
                'method' => 'GET'
            ],
            [
                'pattern' => 'api/v1/books/{book_id}',
                'action' => BookController::class . '@show',
                'method' => 'GET'
            ],
            [
                'pattern' => 'api/v1/books/{book_id}',
                'action' => BookController::class . '@update',
                'method' => 'PATCH'
            ],
            [
                'pattern' => 'api/v1/authors/{author_id}/books',
                'action' => AuthorController::class . '@getBooks',
                'method' => 'GET'
            ],
            [
                'pattern' => 'api/v1/authors',
                'action' => AuthorController::class . '@create',
                'method' => 'POST'
            ],
        ];
    }

    /**
     * @dataProvider getActions
     * @param $pattern
     * @param $action
     */
    public function testActions($pattern, $action, $method)
    {
        $routes = Route::getRoutes();
        $route = $routes->getByAction($action);
        $this->assertNotNull($route);
        $this->assertEquals($pattern, $route->uri);
        $this->assertTrue(in_array($method, $route->methods));
    }
}
