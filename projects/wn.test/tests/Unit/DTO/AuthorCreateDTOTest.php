<?php

use App\DTO\AuthorCreateDTO;
use App\Http\Requests\Api\V1\AuthorCreateRequest;
use Faker\Factory;
use Spatie\DataTransferObject\DataTransferObjectError;
use Tests\TestCase;

class AuthorCreateDTOTest extends TestCase
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * AuthorCreateRequestTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    /**
     * @test
     * @param $passed
     * @param $data
     * @dataProvider getDataFieldValidation
     */
    public function dtoValidation($passed, $data)
    {
        if (!$passed) {
            $this->expectException(DataTransferObjectError::class);
        }

        $request = new AuthorCreateRequest([], [], $data);
        $dto = AuthorCreateDTO::createFromRequest($request);
        $this->assertInstanceOf(AuthorCreateDTO::class, $dto);
    }

    /**
     * @return array[]
     */
    public function getDataFieldValidation()
    {
        return [
            'positive_test' => [
                'passed' => true,
                'data' => [
                    'name' => $this->faker->text,
                    'authors' => $this->faker->randomElements([
                        $this->faker->randomDigit(),
                        $this->faker->randomDigit(),
                        $this->faker->randomDigit(),
                    ])
                ],
            ],
            'empty_data' => [
                'passed' => false,
                'data' => [
                ],
            ],
            'empty_name' => [
                'passed' => false,
                'data' => [
                    'name' => null
                ],
            ],
            'name_not_string' => [
                'passed' => false,
                'data' => [
                    'name' => $this->faker->randomNumber()
                ],
            ],
        ];
    }
}
