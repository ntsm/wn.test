<?php

use App\Http\Requests\Api\V1\BookListRequest;
use Faker\Factory;
use Spatie\DataTransferObject\DataTransferObjectError;
use Tests\TestCase;
use App\DTO\BookListDTO;

class BookListDTOTest extends TestCase
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * AuthorCreateRequestTest constructor.
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    /**
     * @test
     * @param $passed
     * @param $data
     * @dataProvider getDataFieldValidation
     */
    public function dtoValidation($passed, $data)
    {
        if (!$passed) {
            $this->expectException(DataTransferObjectError::class);
        }

        $request = new BookListRequest([], [], $data);
        $dto = BookListDTO::createFromRequest($request);
        $this->assertInstanceOf(BookListDTO::class, $dto);
    }

    /**
     * @return array[]
     */
    public function getDataFieldValidation()
    {
        return [
            'positive_test' => [
                'passed' => true,
                'data' => [
                    'offset' => $this->faker->randomDigit()
                ],
            ],
            'positive_test2' => [
                'passed' => true,
                'data' => [
                    'offset' => $this->faker->text()
                ],
            ],
            'positive_test3' => [
                'passed' => true,
                'data' => [
                ],
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getDtoCastData
     */
    public function dtoCast($actual, $expected)
    {
        $request = new BookListRequest([], [], $actual);
        $dto = BookListDTO::createFromRequest($request);

        $this->assertEquals($expected, $dto->toArray());
    }

    public function getDtoCastData()
    {
        return [
            'text_fields' => [
                'actual' => [
                    'offset' => $this->faker->text,
                    'limit' => $this->faker->text
                ],
                'expected' => [
                    'offset' => 0,
                    'limit' => 0
                ],
            ],
            'empty_fields' => [
                'actual' => [
                ],
                'expected' => [
                    'offset' => 0,
                    'limit' => 100
                ],
            ],
        ];
    }
}
