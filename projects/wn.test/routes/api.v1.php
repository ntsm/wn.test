<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'App\Http\Controllers\Api\V1', 'prefix' => 'v1', 'middleware' => []], function () {
    Route::group(['prefix' => 'books'], function () {
        Route::post('/', 'BookController@create');
        Route::get('/', 'BookController@list');
        Route::get('/{book_id}', 'BookController@show');
        Route::patch('/{book_id}', 'BookController@update');
    });
    Route::group(['prefix' => 'authors'], function () {
        Route::post('/', 'AuthorController@create');
        Route::get('/{author_id}/books', 'AuthorController@getBooks');
    });
});
