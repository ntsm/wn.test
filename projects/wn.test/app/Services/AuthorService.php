<?php

namespace App\Services;

use App\DTO\AuthorCreateDTO;
use App\Models\Author;
use App\Models\Book;
use DB;

/**
 * Class AuthorService
 * @package App\Services
 */
class AuthorService
{
    /**
     * @param AuthorCreateDTO $dto
     * @return Author
     */
    public function create(AuthorCreateDTO $dto)
    {
        $model = new Author();

        $model->name = $dto->name;

        DB::transaction(function () use ($model, $dto) {
            $model->save();
            $model = $this->syncBooks($model, $dto->books);
        });

        return $model;
    }

    /**
     * @param $authorId
     * @return mixed
     */
    public function get($authorId)
    {
        return Author::findOrFail($authorId);
    }

    /**
     * @param $authorId
     * @return Book[]
     */
    public function getAuthorBooks($authorId)
    {
        return $this->get($authorId)->books;
    }

    /**
     * @param Author $model
     * @param $authorIds
     * @return Author
     */
    private function syncBooks(Author $model, $bookIds)
    {
        if (!empty($bookIds)) {
            $model->books()->sync($bookIds);
        }

        $model->load('books');

        return $model;
    }
}
