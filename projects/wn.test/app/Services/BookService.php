<?php

namespace App\Services;

use App\DTO\BookUpsertDTO;
use App\DTO\BookListDTO;
use App\Models\Book;
use DB;

/**
 * Class BookService
 * @package App\Services
 */
class BookService
{
    /**
     * @param BookUpsertDTO $dto
     * @return Book
     */
    public function create(BookUpsertDTO $dto)
    {
        $model = new Book();
        $model->title = $dto->title;

        DB::transaction(function () use ($model, $dto) {
            $model->save();
            $model = $this->syncAuthors($model, $dto->authors);
        });

        return $model;
    }

    /**
     * @param Book $model
     * @param BookUpsertDTO $dto
     * @return Book
     */
    public function update(Book $model, BookUpsertDTO $dto)
    {
        $model->title = $dto->title;

        DB::transaction(function () use ($model, $dto) {
            if ($model->isDirty()) {
                $model->save();
            }

            $model = $this->syncAuthors($model, $dto->authors);
        });

        return $model;
    }

    /**
     * @param int $bookId
     * @return mixed
     */
    public function get(int $bookId)
    {
        return Book::with('authors')->findOrFail($bookId);
    }

    /**
     * @param BookListDTO $dto
     * @return Book[]
     */
    public function list(BookListDTO $dto)
    {
        return Book::query()
            ->limit($dto->limit)
            ->offset($dto->offset)
            ->get();
    }

    /**
     * @param Book $model
     * @param $authorIds
     * @return Book
     */
    private function syncAuthors(Book $model, $authorIds)
    {
        if (!empty($authorIds)) {
            $model->authors()->sync($authorIds);
        }

        $model->load('authors');

        return $model;
    }
}
