<?php

namespace App\Http\Resources\Api\V1;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AuthorResource
 * @package App\Http\Resources\Api\V1
 */
class AuthorResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            $this->mergeWhen(
                $this->whenLoaded('books'),
                [
                    'books' => $this->whenLoaded('books')
                ]
            ),
        ];
    }
}
