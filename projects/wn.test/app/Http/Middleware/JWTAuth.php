<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\TransformsRequest;
use Illuminate\Validation\UnauthorizedException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JWTAuth extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = $this->auth->parser()->setRequest($request);

        if (!$auth->hasToken() || $auth->parseToken() !== env('JWT_SECRET')) {
            throw new UnauthorizedException('Token not provided.');
        }

        return $next($request);
    }
}
