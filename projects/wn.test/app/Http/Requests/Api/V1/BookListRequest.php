<?php

namespace App\Http\Requests\Api\V1;

use Illuminate\Foundation\Http\FormRequest;

class BookListRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'offset' => 'integer',
        ];
    }
}
