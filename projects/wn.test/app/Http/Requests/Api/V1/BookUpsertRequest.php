<?php

namespace App\Http\Requests\Api\V1;

use Illuminate\Foundation\Http\FormRequest;

class BookUpsertRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255|min:2',
            'authors' => 'array',
            'authors.*' => 'integer'
        ];
    }
}
