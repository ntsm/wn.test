<?php

namespace App\Http\Requests\Api\V1;

use Illuminate\Foundation\Http\FormRequest;

class AuthorCreateRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|min:2',
            'books' => 'array',
            'books.*' => 'integer'
        ];
    }
}
