<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class DocsController extends Controller
{
    /**
     * @param Request $request
     * @param string $version
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $version)
    {
        return view('docs', compact('version'));
    }

    /**
     * @param Request $request
     * @param string $version
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getDocsFile(Request $request, $version)
    {
        return new BinaryFileResponse(
            base_path("docs/$version.yaml"),
            200,
            ['Content-Type' => 'text/yaml', 'Access-Control-Allow-Origin' => ['*']]
        );
    }
}
