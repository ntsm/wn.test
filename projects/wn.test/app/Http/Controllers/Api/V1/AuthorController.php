<?php

namespace App\Http\Controllers\Api\V1;

use App\DTO\AuthorCreateDTO;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\V1\AuthorCreateRequest;
use App\Http\Resources\Api\V1\AuthorResource;
use App\Http\Resources\Api\V1\BookCollection;
use App\Services\AuthorService;

/**
 * Class AuthorController
 * @package App\Http\Controllers\Api\V1
 */
class AuthorController extends ApiController
{
    /** @var AuthorService  */
    private $authorService;

    public function __construct(AuthorService $service)
    {
        $this->authorService = $service;
    }

    /**
     * @param AuthorCreateRequest $request
     * @return mixed
     */
    public function create(AuthorCreateRequest $request)
    {
        $dto = AuthorCreateDTO::createFromRequest($request);

        return response()->api(
            new AuthorResource($this->authorService->create($dto))
        );
    }

    /**
     * @param $authorId
     * @return mixed
     */
    public function getBooks($authorId)
    {
        return response()->api(
            new BookCollection($this->authorService->getAuthorBooks($authorId))
        );
    }
}
