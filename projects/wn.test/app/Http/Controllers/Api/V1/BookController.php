<?php

namespace App\Http\Controllers\Api\V1;

use App\DTO\BookUpsertDTO;
use App\DTO\BookListDTO;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\V1\BookUpsertRequest;
use App\Http\Requests\Api\V1\BookListRequest;
use App\Http\Resources\Api\V1\BookCollection;
use App\Http\Resources\Api\V1\BookResource;
use App\Services\BookService;

/**
 * Class AuthorController
 * @package App\Http\Controllers\Api\V1
 */
class BookController extends ApiController
{
    /** @var BookService  */
    private $bookService;

    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * @param BookUpsertRequest $request
     * @return mixed
     */
    public function create(BookUpsertRequest $request)
    {
        $dto = BookUpsertDTO::createFromRequest($request);

        return response()->api(
            new BookResource(
                $this->bookService->create($dto)
            )
        );
    }

    /**
     * @param BookUpsertRequest $request
     * @return mixed
     */
    public function update(BookUpsertRequest $request, $bookId)
    {
        $dto = BookUpsertDTO::createFromRequest($request);
        $model = $this->bookService->get($bookId);

        return response()->api(
            new BookResource(
                $this->bookService->update($model, $dto)
            )
        );
    }

    /**
     * @param BookListRequest $request
     * @return mixed
     */
    public function list(BookListRequest $request)
    {
        $dto = BookListDTO::createFromRequest($request);

        return response()->api(
            new BookCollection(
                $this->bookService->list($dto)
            )
        );
    }

    /**
     * @param $bookId
     * @return mixed
     */
    public function show($bookId)
    {
        return response()->api(
            new BookResource(
                $this->bookService->get($bookId)
            )
        );
    }
}
