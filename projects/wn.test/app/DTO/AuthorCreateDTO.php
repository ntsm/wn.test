<?php

namespace App\DTO;

use App\Http\Requests\Api\V1\AuthorCreateRequest;
use Spatie\DataTransferObject\DataTransferObject;

class AuthorCreateDTO extends DataTransferObject
{
    /** @var string */
    public $name;
    /** @var array */
    public $books;

    /**
     * @param AuthorCreateRequest $request
     * @return self
     */
    public static function createFromRequest(AuthorCreateRequest $request)
    {
        return new self(
            [
                'name' => $request->get('name'),
                'books' => $request->get('books', [])
            ]
        );
    }
}
