<?php

namespace App\DTO;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class BookListDTO extends DataTransferObject
{
    /** @var integer */
    public $offset;
    /** @var integer */
    public $limit;

    /**
     * @param Request $request
     * @return self
     */
    public static function createFromRequest(Request $request)
    {
        return new self(
            [
                'offset' => (int) $request->get('offset', 0),
                'limit' => (int) $request->get('limit', 100),
            ]
        );
    }
}
