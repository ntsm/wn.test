<?php

namespace App\DTO;

use App\Http\Requests\Api\V1\BookUpsertRequest;
use Spatie\DataTransferObject\DataTransferObject;

class BookUpsertDTO extends DataTransferObject
{
    /** @var string */
    public $title;
    /** @var array */
    public $authors;

    /**
     * @param BookUpsertRequest $request
     * @return self
     */
    public static function createFromRequest(BookUpsertRequest $request)
    {
        return new self(
            [
                'title' => $request->get('title'),
                'authors' => $request->get('authors', [])
            ]
        );
    }
}
