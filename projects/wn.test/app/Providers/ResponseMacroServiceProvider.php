<?php

namespace App\Providers;

use App\Acme\ResponseErrorCode;
use ArrayObject;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

/**
 * Class ResponseMacroServiceProvider
 */
class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('api', function ($data = [], $status = null, $statusCode = 200) {
            return response()->json(
                [
                    // If data is empty, field must be empty object instead of empty array
                    'data' => empty($data) ? new ArrayObject() : $data,
                    'status' => $status ?? __('statuses.ok'),
                ],
                $statusCode
            );
        });

        Response::macro('error', function ($msg, $status = null, $statusCode = 400) {
            return response()->json(
                [
                    // If data is empty, field must be empty object instead of empty array
                    'data' => [
                        'msg' => $msg
                    ],
                    'status' => $status ?? __('statuses.error'),
                ],
                $statusCode
            );
        });
    }
}
