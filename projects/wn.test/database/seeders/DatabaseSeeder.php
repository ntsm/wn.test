<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $books = Book::factory(10)->create();
        $authors = Author::factory(2)->create();
        $bookModel = Book::find($books->pop()->id);
        $bookModel->authors()->sync($authors->pluck('id')->toArray());
    }
}
