#!/bin/bash

cd services

docker-compose up -d --build --force

docker exec -ti wn_php bash -c 'cp /src/wn/.env.example /src/wn/.env'
docker exec -ti wn_php bash -c 'composer install -d /src/wn'
docker exec -ti wn_php bash -c 'chown -R $USER:www-data /src/wn/storage'
docker exec -ti wn_php bash -c 'chown -R $USER:www-data /src/wn/bootstrap/cache'
docker exec -ti wn_php bash -c 'chmod -R 775 /src/wn/storage'
docker exec -ti wn_php bash -c 'chmod -R 775 /src/wn/bootstrap/cache'
docker exec -ti wn_php bash -c 'php /src/wn/artisan key:generate'
docker exec -ti wn_php bash -c 'php /src/wn/artisan migrate --seed'
docker exec -ti wn_php bash -c 'php /src/wn/artisan migrate:fresh --seed --env=testing'
docker exec -ti wn_php bash -c 'php /src/wn/artisan jwt:secret'
docker exec -ti wn_php bash -c 'php /src/wn/artisan jwt:secret --env=testing'
docker exec -ti wn_php bash -c 'cd /src/wn && php artisan test'